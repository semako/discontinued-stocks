package stocks

import "database/sql/driver"

const (
	Binance  = "BINANCE"
	BitMax   = "BITMAX"
	BitMex   = "BITMEX"
	BitFinex = "BITFINEX"
	BitTrex  = "BITTREX"
)

type Stock string

func (ar *Stock) Scan(value interface{}) error {
	*ar = Stock(value.([]byte))
	return nil
}

func (ar Stock) Value() (driver.Value, error) {
	return string(ar), nil
}

func List() []string {
	return []string{
		Binance,
		BitMax,
		BitMex,
		BitFinex,
		BitTrex,
	}
}

func IsValid(s string) bool {
	for _, i := range List() {
		if i == s {
			return true
		}
	}

	return false
}
